export const successConfig = { color: 'teal', icon: 'tag_faces' }
export const errorConfig = { color: 'negative', icon: 'report_problem' }