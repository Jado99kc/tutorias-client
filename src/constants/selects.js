export const CUATRIMESTRES = [1, 2, 3, 4, 5, 6, 7, 8, 9];
export const TURNOS = ["Matutino", "Vespertino"];
export const TIPOS_TUTORIA = ["Academica", "Administrativa", "Personal"];
export const TIPOS_REPORTE = [
  { tipo: 1, nombre: "REPORTE 1ER PARCIAL DEL CUATRIMESTRE" },
  { tipo: 2, nombre: "REPORTE 2DO PARCIAL DEL CUATRIMESTRE " },
  { tipo: 3, nombre: "REPORTE FINAL CUATRIMESTRAL" },
];
export const TIPOS_TRABAJO = ["Permanente", "Temporal"];
export const TIPOS_BAJA = ["Permanente", "Temporal"];
export const SEXOS = ["Masculino", "Femenino", "Otro"];
export const DEJO_DE_ASISTIR = [
  {
    text: "Si",
    value: true,
  },
  { text: "No", value: false },
];
export const CULMINO = [
  {
    text: "Si",
    value: true,
  },
  { text: "No", value: false },
];
