import { formatDate } from "src/filters/date";
export const downloadFinalPDF = (blob, reporte) => {
  const url = window.URL.createObjectURL(new Blob([blob]));
  const link = document.createElement("a");
  link.href = url;
  link.setAttribute(
    "download",
    `Alumno-${reporte.nombre_completo}/fecha-${formatDate(
      reporte.created_at
    )}.pdf`
  );
  document.body.appendChild(link);
  link.click();
};
