import { formatDateCustom } from "src/filters/date";

export const downloadSesionPDF = (blob, reporte) => {
  const url = window.URL.createObjectURL(new Blob([blob]));
  const link = document.createElement("a");
  link.href = url;
  link.setAttribute(
    "download",
    `cuatrimestre-${reporte.cuatrimestre}/grupo-${
      reporte.grupo
    }/${formatDateCustom(reporte.fecha)}.pdf`
  );
  document.body.appendChild(link);
  link.click();
};
export const downloadFinalPDF = (blob, reporte) => {
  const url = window.URL.createObjectURL(new Blob([blob]));
  const link = document.createElement("a");
  link.href = url;
  link.setAttribute(
    "download",
    `cuatrimestre-${reporte.cuatrimestre}/grupo-${
      reporte.grupo
    }/${formatDateCustom(reporte.fecha)}.pdf`
  );
  document.body.appendChild(link);
  link.click();
};
