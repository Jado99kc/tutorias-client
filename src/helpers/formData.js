export const convertToFormData = (object) => {
  let newForm = new FormData();
  for (let key in object) {
    newForm.append(key, object[key]);
  }
  return newForm;
};

export const debugFormData = (formData) => {
  for (var pair of formData.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  }
};
