import { api } from "src/boot/axios";
export const getImage = async (fileName) => {
  try {
    const res = await api.get(`/getImagen/${fileName}`);
    return res.data;
  } catch (error) {
    console.log(error);
  }
};
