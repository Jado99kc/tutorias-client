import { formatDate } from "src/filters/date";
export const downloadPDF = (pdf_data, reporte) => {
  if (!reporte.grupo) {
    const url = window.URL.createObjectURL(new Blob([pdf_data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute(
      "download",
      `${reporte.alumno}/cuatrimestre-${reporte.cuatrimestre}/${formatDate(
        reporte.fecha
      )}.pdf`
    );
    document.body.appendChild(link);
    link.click();
  } else {
    const url = window.URL.createObjectURL(new Blob([pdf_data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute(
      "download",
      `${reporte.carrera}/Grupo-${reporte.grupo}/cuatrimestre-${
        reporte.cuatrimestre
      }/${formatDate(reporte.fecha)}.pdf`
    );
    document.body.appendChild(link);
    link.click();
  }
};
export const downloadParcialPDF = (pdf_data, reporte) => {
  const url = window.URL.createObjectURL(new Blob([pdf_data]));
  const link = document.createElement("a");
  link.href = url;
  if (reporte.tipo == 1) {
    link.setAttribute(
      "download",
      `Reporte Primer Parcial: Grupo-${reporte.grupo}/cuatrimestre-${
        reporte.cuatrimestre
      }/${formatDate(reporte.created_at)}.pdf`
    );
  }
  if (reporte.tipo == 2) {
    link.setAttribute(
      "download",
      `Reporte Segundo Parcial: Grupo-${reporte.grupo}/cuatrimestre-${
        reporte.cuatrimestre
      }/${formatDate(reporte.created_at)}.pdf`
    );
  }
  if (reporte.tipo == 3) {
    link.setAttribute(
      "download",
      `Reporte parcial Final: Grupo-${reporte.grupo}/cuatrimestre-${
        reporte.cuatrimestre
      }/${formatDate(reporte.created_at)}.pdf`
    );
  }

  document.body.appendChild(link);
  link.click();
};
