export function SET_TOKEN(state, token) {
  state.token = token;
}
export function SET_USER(state, user) {
  state.user = user;
}
export function SET_ERROR(state, error) {
  state.error = error;
}

//Perfiles

export function SET_PERFIL(state, perfil) {
  state.user.tutor = perfil;
}
