import { api, axios } from "src/boot/axios";
import { convertToFormData, debugFormData } from "src/helpers/formData";

export async function validateSession({ commit, state }) {
  if (!state.user && !state.token) {
    const token = localStorage.getItem("token");
    // console.log(token, "here");
    if (token) {
      api.defaults.headers.common = { Authorization: `Bearer ${token}` };
      axios.defaults.headers.common = { Authorization: `Bearer ${token}` };
      const res = await api.get("/user");
      // console.log(res.data);
      if (res?.status == 200) {
        // console.log(res.data.user);

        commit("SET_USER", {
          ...res.data.user,
          roles: res.data.roles,
          tutor: res.data.user.tutor,
          admin: res.data.user.admin,
          alumno_monitor: res.data.user.alumno_monitor,
          psicologo: res.data.user.psicologo,
        });
        commit("SET_TOKEN", token);
        return true;
      } else {
        commit("SET_USER", null);
        localStorage.setItem("user", null);
        localStorage.setItem("token", null);
        return false;
      }
    } else {
      console.log("out");
      commit("SET_USER", null);
      return false;
    }
  }
}
export async function login({ commit }, credentials) {
  try {
    const res = await api.post("/login", credentials);
    // console.log(res.data);
    if (res.status == 200) {
      localStorage.setItem("token", res.data.token);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      commit("SET_TOKEN", res.data.token);
      // console.log(res.data.user);
      commit("SET_USER", {
        ...res.data.user,
        roles: res.data.roles,
        admin: res.data.user.admin,
        tutor: res.data.user.tutor,
        alumno_monitor: res.data.user.alumno_monitor,
        psicologo: res.data.user.psicologo,
      });
      axios.defaults.headers.common = {
        Authorization: `Bearer ${res.data.token}`,
      };
      api.defaults.headers.common = {
        Authorization: `Bearer ${res.data.token}`,
      };
      this.$router.push("/");
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return error.message;
  }
}

export async function logout({ commit }) {
  try {
    console.log(api.defaults);
    await api.post("/logout");
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    commit("SET_TOKEN", null);
    commit("SET_USER", null);
    axios.defaults.headers.common = {
      Authorization: "",
    };
    this.$router.push("/login");
  } catch (error) {
    console.log(error);
  }
}

export async function updateTutorPerfil({ commit }, perfil) {
  console.log(perfil);
  const perfilF = convertToFormData(perfil);
  try {
    const res = await api.post("/tutor/updatePerfil", perfilF, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    console.log(res.data);
    if (res.status == 200) {
      commit("SET_PERFIL", res.data);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
