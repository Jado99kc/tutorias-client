export default function () {
  return {
    user: null,
    token: null,
    error: null,
  };
}
