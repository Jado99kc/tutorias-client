import state from "./state";

export function userIsAuthenticated(state) {
  return state.token != null;
}
export function userRoles(state) {
  return state.user.roles;
}
export function primaryRole(state, getters) {
  if (!state.user) {
    return [];
  }
  if (getters.userRoles.includes("Admin")) {
    return "Administrador";
  } else {
    let roles = "";
    for (let i = 0; i < getters.userRoles.length; i++) {
      if (i == getters.userRoles.length) {
        roles.concat(`${getters.userRoles[i]}`);
      }
      roles.concat(`${getters.userRoles[i]} |`);
    }
    return roles;
  }
}

export function currentUserRole(state) {
  if (!state.user) {
    return [];
  }
  return state?.user.roles[0];
}

export function TutorProfile(state) {
  return {
    ...state.user?.tutor,
    email: state.user.email,
    nombreCompleto: `${state.user?.tutor.nombres} ${state.user?.tutor.apellidoP} ${state.user?.tutor.apellidoM}`,
  };
}
export function AdminProfile(state) {
  return { ...state.user?.admin, email: state.user.email };
}
export function MonitorProfile(state) {
  return {
    ...state.user?.alumno_monitor,
    email: state.user.email,
    nombreCompleto: `${state.user?.alumno_monitor.nombres} ${state.user?.alumno_monitor.apellidoP} ${state.user?.alumno_monitor.apellidoM}`,
  };
}
export function PsicologoProfile(state) {
  return {
    ...state.user?.psicologo,
    email: state.user.email,
    nombreCompleto: `${state.user?.psicologo.nombres} ${state.user?.psicologo.apellidoP} ${state.user?.psicologo.apellidoM}`,
  };
}
