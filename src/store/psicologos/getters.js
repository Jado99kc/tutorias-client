export function entrevistas(state) {
  return state.entrevistas?.data;
}

export function paginationObjectE(state) {
  const { data, ...pagination } = state.entrevistas;
  return pagination;
}

export function paginationE(state, getters) {
  const pagination = {
    sortBy: "created_at",
    nameending: false,
    page: getters.paginationObjectE.current_page,
    rowsPerPage: getters.paginationObjectE.per_page,
    rowsNumber: getters.paginationObjectE.total,
    totalPages: getters.paginationObjectE.last_page,
  };
  return pagination;
}

export function reportesF(state) {
  return state.reportesF?.data;
}

export function paginationObjectF(state) {
  const { data, ...pagination } = state.reportesF;
  return pagination;
}

export function paginationRF(state, getters) {
  const pagination = {
    sortBy: "created_at",
    nameending: false,
    page: getters.paginationObjectF.current_page,
    rowsPerPage: getters.paginationObjectF.per_page,
    rowsNumber: getters.paginationObjectF.total,
    totalPages: getters.paginationObjectF.last_page,
  };
  return pagination;
}

export function canalizaciones(state) {
  return state.canalizaciones?.data;
}
export function paginationObjectC(state) {
  const { data, ...pagination } = state.canalizaciones;
  return pagination;
}

export function paginationC(state, getters) {
  const pagination = {
    sortBy: "created_at",
    nameending: false,
    page: getters.paginationObjectC.current_page,
    rowsPerPage: getters.paginationObjectC.per_page,
    rowsNumber: getters.paginationObjectC.total,
    totalPages: getters.paginationObjectC.last_page,
  };
  return pagination;
}
