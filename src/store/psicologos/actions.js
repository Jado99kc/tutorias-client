import { api, axios } from "src/boot/axios";
import { convertToFormData } from "src/helpers/formData";
import { downloadFinalPDF } from "src/helpers/PsicologoPDF";
export async function uploadEntrevista({ commit }, file) {
  try {
    const fileF = convertToFormData({ file: file });
    const res = await api.post("/psicologo/uploadEntrevista", fileF, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    console.log(res);
    if (res.status == 200) {
      console.log(res.data);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function getEntrevistas({ commit }) {
  try {
    const res = await api.get("/psicologo/getEntrevistas");
    console.log(res);
    if (res.status == 200) {
      console.log(res.data);
      commit("SET_ENTREVISTAS", res.data);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function getFormatoEntrevistaURL() {
  try {
    const res = await api.get("/psicologo/getFormatoEntrevistaURL");
    if (res.status == 200) {
      return res.data;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function getReportesFinales({ commit }) {
  try {
    const res = await api.get("/psicologo/getReportesFinales");
    if (res.status == 200) {
      commit("SET_REPORTES_FINALES", res.data);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function printReporteFinal({}, reporte) {
  try {
    const res = await api.get(`/psicologo/printReporteFinal/${reporte.id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      downloadFinalPDF(res.data, reporte);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function createReporteFinal({}, reporte) {
  try {
    const res = await api.post("/psicologo/createReporteFinal", reporte);
    if (res.status == 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function getReporteFinal({}, id) {
  try {
    const res = await api.get(`/psicologo/getReporteFinal/${id}`);
    if (res.status == 200) {
      return res.data;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function updateReporteFinal({}, reporte) {
  try {
    const res = await api.post(
      `/psicologo/updateReporteFinal/${reporte.id}`,
      reporte
    );
    if (res.status == 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function getCanalizaciones({ commit }) {
  try {
    const res = await api.get("/psicologo/canalizaciones");
    if (res.status == 200) {
      commit("SET_CANALIZACIONES", res.data);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
