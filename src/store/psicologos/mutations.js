export function SET_PSICOLOGO(state, psicologo) {
  state.psicologo = psicologo;
}
export function ADD_PSICOLOGO(state, psicologo) {
  state.psicologos.push(psicologo);
}
export function UPDATE_PSICOLOGO(state, psicologo) {
  state.psicologos = state.psicologos.map((x) =>
    x.id == psicologo.id ? psicologo : x
  );
}
export function SET_ENTREVISTAS(state, entrevistas) {
  state.entrevistas = entrevistas;
}
export function SET_REPORTES_FINALES(state, reportes) {
  state.reportesF = reportes;
}
export function SET_CANALIZACIONES(state, canalizaciones) {
  state.canalizaciones = canalizaciones;
}
