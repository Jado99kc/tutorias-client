export default function () {
  return {
    tutor: null,
    reportesI: null,
    reportesG: null,
    reportesP: null,
    alumnosM: null,
    alumnosMD: null,
    alumnosT: null,
    alumnosTD: null,
    reportesFM: null,
  };
}
