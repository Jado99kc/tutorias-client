//*Tutor

export function tutorId(state, getters, rootState) {
  return rootState.auth.user?.tutor.id;
}

//*REPORTES INDIVIDUALES

export function reportesI(state) {
  return state.reportesI?.data;
}

export function paginationObjectI(state) {
  const { data, ...pagination } = state.reportesI;
  return pagination;
}
export function paginationI(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectI.current_page,
    rowsPerPage: getters.paginationObjectI.per_page,
    rowsNumber: getters.paginationObjectI.total,
    totalPages: getters.paginationObjectI.last_page,
  };
  return pagination;
}
//* Reportes Grupales
export function reportesG(state) {
  return state.reportesG?.data;
}

export function paginationObjectG(state) {
  const { data, ...pagination } = state.reportesG;
  return pagination;
}
export function paginationG(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectG.current_page,
    rowsPerPage: getters.paginationObjectG.per_page,
    rowsNumber: getters.paginationObjectG.total,
    totalPages: getters.paginationObjectG.last_page,
  };
  return pagination;
}
//*Reportes Parciales
export function reportesP(state) {
  return state.reportesP?.data;
}

export function paginationObjectP(state) {
  const { data, ...pagination } = state.reportesP;
  return pagination;
}
export function paginationP(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectP.current_page,
    rowsPerPage: getters.paginationObjectP.per_page,
    rowsNumber: getters.paginationObjectP.total,
    totalPages: getters.paginationObjectP.last_page,
  };
  return pagination;
}
//* Alumnos Monitores
export function alumnosM(state) {
  return state.alumnosM?.data;
}

export function paginationObjectAM(state) {
  const { data, ...pagination } = state.alumnosM;
  return pagination;
}
export function paginationAM(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectAM.current_page,
    rowsPerPage: getters.paginationObjectAM.per_page,
    rowsNumber: getters.paginationObjectAM.total,
    totalPages: getters.paginationObjectAM.last_page,
  };
  return pagination;
}
//* Alumnos Monitores
export function alumnosMD(state) {
  return state.alumnosMD?.data;
}

export function paginationObjectAMD(state) {
  const { data, ...pagination } = state.alumnosMD;
  return pagination;
}
export function paginationAMD(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectAMD.current_page,
    rowsPerPage: getters.paginationObjectAMD.per_page,
    rowsNumber: getters.paginationObjectAMD.total,
    totalPages: getters.paginationObjectAMD.last_page,
  };
  return pagination;
}
//* Alumnos Tutorados
export function alumnosTD(state) {
  return state.alumnosTD?.data;
}

export function paginationObjectATD(state) {
  const { data, ...pagination } = state.alumnosTD;
  return pagination;
}
export function paginationATD(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectATD.current_page,
    rowsPerPage: getters.paginationObjectATD.per_page,
    rowsNumber: getters.paginationObjectATD.total,
    totalPages: getters.paginationObjectATD.last_page,
  };
  return pagination;
}
export function alumnosT(state) {
  return state.alumnosT?.data;
}

export function paginationObjectAT(state) {
  const { data, ...pagination } = state.alumnosT;
  return pagination;
}
export function paginationAT(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectAT.current_page,
    rowsPerPage: getters.paginationObjectAT.per_page,
    rowsNumber: getters.paginationObjectAT.total,
    totalPages: getters.paginationObjectAT.last_page,
  };
  return pagination;
}

export function reportesFM(state) {
  return state.reportesFM?.data;
}

export function paginationObjectFM(state) {
  const { data, ...pagination } = state.reportesFM;
  return pagination;
}

export function paginationFM(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectFM.current_page,
    rowsPerPage: getters.paginationObjectFM.per_page,
    rowsNumber: getters.paginationObjectFM.total,
    totalPages: getters.paginationObjectFM.last_page,
  };
  return pagination;
}
