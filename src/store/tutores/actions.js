import { api, axios } from "src/boot/axios";
import { downloadPDF, downloadParcialPDF } from "src/helpers/pdf";
export function getTutores({ commit }) {
  try {
    //async fetch
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export function updateTutor({ commit }, tutor) {
  try {
    //async fetch PUT
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function printReporte({}, reporte) {
  // return console.log(reporte, "front");
  try {
    let res = null;
    if (!reporte.grupo) {
      res = await api.get(`/printRepInd/${reporte.id}`, {
        responseType: "blob",
      });
    } else {
      res = await api.get(`/printRepGrup/${reporte.id}`, {
        responseType: "blob",
      });
    }

    if (res.status == 200) {
      //!commit to tutores
      downloadPDF(res.data, reporte);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function printReporteParcial({}, reporte) {
  try {
    const res = await api.get(`/tutor/generarReporteParcial/${reporte.id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      downloadParcialPDF(res.data, reporte);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

//* Reportes Individuales
export async function getReporteI({}, id) {
  try {
    const res = await api.get(`/tutor/reportesIndividuales/${id}`);
    return res.data;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function previewReporteIndividual({}, id) {
  try {
    const res = await api.get(`/printRepInd/${id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      return URL.createObjectURL(res.data);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function updateReporteI({}, reporte) {
  try {
    const res = await api.post(
      `/tutor/reporteIndUpdate/${reporte.id}`,
      reporte
    );
    if (res.status == 200) {
      //!commit to reportes
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function createReporteI({}, reporte) {
  try {
    const res = await api.post("/tutor/crearReporteIndividual", reporte);
    if (res.status == 200) {
      //!commit to reportes
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function getReportesI({ commit, state }, id) {
  try {
    const res = await api.get(`/tutor/${id}/reportesIndividuales`);
    commit("SET_REPORTESI_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}

export async function changePaginationI({ commit, state }, direction) {
  console.log(direction);
  try {
    let res = [];
    if (direction == "next") {
      res = await axios.get(state.reportesI.next_page_url);
    }
    if (direction == "prev") {
      res = await axios.get(state.reportesI.prev_page_url);
    }
    if (direction == "first") {
      res = await axios.get(state.reportesI.first_page_url);
    }
    if (direction == "last") {
      res = await axios.get(state.reportesI.last_page_url);
    }
    commit("SET_REPORTESI_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}

//* Reportes Grupales
export async function getReporteG({}, id) {
  try {
    const res = await api.get(`/tutor/reportesGrupales/${id}`);
    return res.data;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function previewReporteGrupal({}, id) {
  try {
    const res = await api.get(`/printRepGrup/${id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      return URL.createObjectURL(res.data);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function updateReporteG({}, reporte) {
  try {
    const res = await api.post(
      `/tutor/reporteGrupUpdate/${reporte.id}`,
      reporte
    );
    if (res.status == 200) {
      //!commit to reportes
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function createReporteG({}, reporte) {
  try {
    const res = await api.post("/tutor/crearReporteGrupal", reporte);
    if (res.status == 200) {
      //!commit to reportes
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function getReportesG({ commit, state }, id) {
  try {
    const res = await api.get(`/tutor/${id}/reportesGrupales`);
    commit("SET_REPORTESG_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}

export async function changePaginationG({ commit, state }, direction) {
  //!Module represent store module name
  try {
    let res = [];
    if (direction == "next") {
      res = await axios.get(state.reportesG.next_page_url);
    }
    if (direction == "prev") {
      res = await axios.get(state.reportesG.prev_page_url);
    }
    if (direction == "first") {
      res = await axios.get(state.reportesG.first_page_url);
    }
    if (direction == "last") {
      res = await axios.get(state.reportesG.last_page_url);
    }
    commit("SET_REPORTESG_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
//* Reportes Parciales

export async function crearReporteParcial({}, reporteData) {
  // return console.log(reporteData);
  console.log(reporteData);
  try {
    const res = await api.post("/tutor/crearReporteParcial", reporteData);
    if (res.status == 200) {
      //commit
      return true;
    }
  } catch (error) {
    console.log(error);
    return true;
  }
}
export async function updateReporteParcial({}, reporteData) {
  // return console.log(reporteData);
  try {
    const res = await api.post(
      `/tutor/reporteParcial/update/${reporteData.id}`,
      reporteData
    );
    if (res.status == 200) {
      //commit
      return true;
    }
  } catch (error) {
    console.log(error);
    return true;
  }
}
export async function getReportesParciales({ commit }, tipo) {
  try {
    const res = await api.get(`/tutor/reportesParciales/${tipo}`);
    console.log(res.data);
    commit("SET_REPORTESP_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function getReportesParcialesAdmin({ commit }, params) {
  try {
    const res = await api.get(
      `/tutor/${params.id}/reportesParciales/${params.tipo}`
    );
    console.log(res.data);
    commit("SET_REPORTESP_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function getReporteP({ getters }, id) {
  try {
    if (getters.reportesP) {
      return getters.reportesP.find((x) => x.id == id);
    } else {
      const res = await api.get(`/tutor/reporteParcial/${id}`);
      return res.data;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function previewReporteParcial({}, id) {
  try {
    const res = await api.get(`/tutor/generarReporteParcial/${id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      return URL.createObjectURL(res.data);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function removeAlumnoReprobadoFromReporte({}, id) {
  try {
    const res = await api.post(
      `/tutor/reporteParcial/removeAlumnoReprobado/${id}`
    );
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function removeAlumnoLaboraFromReporte({}, id) {
  try {
    const res = await api.post(
      `/tutor/reporteParcial/removeAlumnoLabora/${id}`
    );
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function removeAlumnoBajaFromReporte({}, id) {
  try {
    const res = await api.post(`/tutor/reporteParcial/removeAlumnoBaja/${id}`);
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

//* Alumnos Monitores
export async function getAlumnoM({ commit, getters }, id) {
  try {
    if (getters.alumnosM) {
      const monitor = getters.alumnosM.find((x) => x.id == id);
      if (monitor) {
        return monitor;
      } else {
        const res = await api.get(`/tutor/alumnosMonitores/${id}`);
        return res.data ? res.data : null;
      }
    } else {
      const res = await api.get(`/tutor/alumnosMonitores/${id}`);
      return res.data ? res.data : null;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function getAlumnosM({ commit }, id) {
  try {
    const res = await api.get(`/tutor/${id}/alumnosMonitores`);
    commit("SET_ALUMNOSM_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function updateAlumnoM({ commit }, alumno) {
  // return console.log(alumno);
  try {
    const res = await api.post(
      `/tutor/alumnosMonitores/${alumno.id}/update`,
      alumno
    );
    return res.status == 200;
    //!commit
  } catch (error) {
    console.log(error);
  }
}
export async function deshabilitarMonitor({ commit }, alumno) {
  try {
    const res = await api.post(`/tutor/desactivarMonitor/${alumno.id}`);
    if (res.status == 200) {
      commit("DESHABILITAR_MONITOR", alumno);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function habilitarMonitor({ commit }, alumno) {
  try {
    const res = await api.post(`/tutor/activarMonitor/${alumno.id}`);
    if (res.status == 200) {
      commit("HABILITAR_MONITOR", alumno);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function getAlumnosMD({ commit }, id) {
  //!
  try {
    const res = await api.get(`/tutor/${id}/alumnosMonitoresDesactivados`);
    commit("SET_ALUMNOSMD_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function createAlumnoM({ commit, getters }, alumno) {
  alumno.tutor_id = getters.tutorId; //!set tutor id from state
  try {
    const res = await api.post("/tutor/storeMonitor", alumno);
    if (res.status == 200) {
      //!commit to tutores
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
//*Alumnos Tutorados
export async function getAlumnoT({ commit, getters }, id) {
  try {
    if (getters.alumnosT) {
      const tutorado = getters.alumnosT.find((x) => x.id == id);
      if (tutorado) {
        return tutorado;
      } else {
        const res = await api.get(`/tutor/alumnosTutorados/${id}`);
        return res.data ? res.data : null;
      }
    } else {
      const res = await api.get(`/tutor/alumnosTutorados/${id}`);
      return res.data ? res.data : null;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function updateAlumnoT({ commit }, alumno) {
  // return console.log(alumno);
  try {
    const res = await api.post(
      `/tutor/alumnosTutorados/${alumno.id}/update`,
      alumno
    );
    return res.status == 200;
    //!commit
  } catch (error) {
    console.log(error);
  }
}
export async function getAlumnosTGeneral({ commit }, id) {
  // console.log("aqui estoy en general", id);
  try {
    const res = await api.get(`/tutor/${id}/alumnosTutorados`);
    console.log(res.data);
    commit("SET_ALUMNOST_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function getAlumnosT({ commit }, id) {
  console.log("aqui estoy", id);
  try {
    const res = await api.get(`/monitor/${id}/alumnosTutorados`);
    commit("SET_ALUMNOST_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function createAlumnoT({ commit }, alumno) {
  try {
    const res = await api.post("/tutor/storeTutorado", alumno);
    if (res.status == 200) {
      //!commit to tutores
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function deshabilitarTutorado({ commit }, alumno) {
  try {
    const res = await api.post(`/tutor/desactivarTutorado/${alumno.id}`);
    if (res.status == 200) {
      commit("DESHABILITAR_TUTORADO", alumno);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function getAlumnosTDGeneral({ commit }, id) {
  //!
  try {
    const res = await api.get(`/tutor/${id}/alumnosTutoradosDesactivados`);
    // console.log(res.data);
    commit("SET_ALUMNOSTD_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function habilitarTutorado({ commit }, alumno) {
  try {
    const res = await api.post(`/tutor/activarTutorado/${alumno.id}`);
    if (res.status == 200) {
      commit("HABILITAR_TUTORADO", alumno);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function getReportesFinalesMonitor({ commit }) {
  try {
    const res = await api.get(`/tutor/reportesFinalesMonitor`);
    commit("SET_REPORTES_FINALES_MONITOR", res.data);
  } catch (error) {
    console.log(error);
  }
}

export async function updateReporteFinalMonitor({ commit }, reporte) {
  try {
    const res = await api.post(
      `/tutor/reportesFinalesMonitor/${reporte.id}/update`,
      reporte
    );
    return res.status == 200;
  } catch (error) {
    console.log(error);
  }
}
