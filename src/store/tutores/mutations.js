import state from "../auth/state";

export function SET_TUTOR(state, tutor) {
  state.tutor = tutor;
}
export function ADD_TUTOR(state, tutor) {
  state.tutores.push(tutor);
}
export function UPDATE_TUTOR(state, tutor) {
  state.tutores = state.tutores.map((x) => (x.id == tutor.id ? tutor : x));
}
export function SET_REPORTESI_DATA(state, reportesData) {
  state.reportesI = reportesData;
}
export function SET_REPORTESG_DATA(state, reportesData) {
  state.reportesG = reportesData;
}

export function SET_ALUMNOSM_DATA(state, alumnos) {
  state.alumnosM = alumnos;
}
export function SET_ALUMNOSMD_DATA(state, alumnos) {
  state.alumnosMD = alumnos;
}
export function SET_ALUMNOST_DATA(state, alumnos) {
  state.alumnosT = alumnos;
}
export function SET_ALUMNOSTD_DATA(state, alumnos) {
  state.alumnosTD = alumnos;
}
export function SET_REPORTESP_DATA(state, reportesData) {
  state.reportesP = reportesData;
}

export function DESHABILITAR_MONITOR(state, alumno) {
  state.alumnosM.data = state.alumnosM.data.filter((x) => x.id != alumno.id);
}
export function DESHABILITAR_TUTORADO(state, alumno) {
  state.alumnosT.data = state.alumnosT.data.filter((x) => x.id != alumno.id);
}
export function HABILITAR_MONITOR(state, alumno) {
  if (state.alumnosMD) {
    state.alumnosMD.data = state.alumnosMD.data.filter(
      (x) => x.id != alumno.id
    );
  }
  if (state.tutores) {
    state.alumnosM.data.unshift(alumno);
  }
}
export function HABILITAR_TUTORADO(state, alumno) {
  if (state.alumnosTD) {
    state.alumnosTD.data = state.alumnosTD.data.filter(
      (x) => x.id != alumno.id
    );
  }
  if (state.tutores) {
    state.alumnosT.data.unshift(alumno);
  }
}

// seprate function for tutorado search monitor select

export function PUSH_MONITOR(state, alumno) {
  state.alumnosM.data.push(alumno);
}

export function SET_REPORTES_FINALES_MONITOR(state, reportes) {
  state.reportesFM = reportes;
}
