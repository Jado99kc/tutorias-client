import { api, axios } from "src/boot/axios";

export async function getCarreras({ commit }) {
  try {
    const res = await api.get("/carreras");
    commit("SET_CARRERAS", res.data);
  } catch (error) {
    console.log(error);
  }
}

export async function changePagination({ commit, rootState }, info) {
  try {
    let res = [];
    if (info.direction == "next") {
      res = await axios.get(
        rootState[info.module][info.collection].next_page_url
      );
    }
    if (info.direction == "prev") {
      res = await axios.get(
        rootState[info.module][info.collection].prev_page_url
      );
    }
    if (info.direction == "first") {
      res = await axios.get(
        rootState[info.module][info.collection].first_page_url
      );
    }
    if (info.direction == "last") {
      res = await axios.get(
        rootState[info.module][info.collection].last_page_url
      );
    }
    commit(`${info.module}/${info.commit}`, res.data, { root: true });
  } catch (error) {
    console.log(error);
  }
}

export async function search({ commit, rootState }, parameters) {
  console.log(parameters);
  try {
    const res = await api.post("/admin/search", {
      table: parameters.table,
      field: parameters.field,
      term: parameters.term,
      active: parameters.active,
    });
    console.log(res.data, "in search");
    if (res.data) {
      console.log("inside");
      commit(`${parameters.module}/${parameters.commit}`, res.data, {
        root: true,
      });
    } else {
      console.log("outside");
      commit(
        `${parameters.module}/${parameters.commit}`,
        { data: [] },
        {
          root: true,
        }
      );
    }
  } catch (error) {
    console.log(error);
  }
}
