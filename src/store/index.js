import { store } from "quasar/wrappers";
import { createStore } from "vuex";

import auth from "./auth";
import monitores from "./monitores";
import psicologos from "./psicologos";
import tutores from "./tutores";
import admin from "./admin";
import general from "./general";

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      auth,
      psicologos,
      tutores,
      admin,
      general,
      monitores,
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING,
  });

  return Store;
});
