export default function () {
  return {
    reportesDataI: null,
    reportesDataG: null,
    tutores: null,
    psicologos: null,
    anuncios: null,

    //Desactivados

    tutoresD: null,
    psicologosD: null,

    //Canalizaciones
    canalizaciones: null,
    //Administradores
    administradores: null,
  };
}
