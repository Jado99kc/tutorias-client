import { api, axios } from "src/boot/axios";
import { convertToFormData } from "src/helpers/formData";

export async function getReportesI({ commit, state }, params) {
  try {
    if (state.reportesDataI == null || params?.force) {
      const res = await api.get("/admin/reportesIndividuales");
      commit("SET_REPORTESI_DATA", res.data);
    }
  } catch (error) {
    console.log(error);
  }
}

export async function changePaginationI({ commit, state }, direction) {
  console.log(direction);
  try {
    let res = [];
    if (direction == "next") {
      res = await axios.get(state.reportesDataI.next_page_url);
    }
    if (direction == "prev") {
      res = await axios.get(state.reportesDataI.prev_page_url);
    }
    if (direction == "first") {
      res = await axios.get(state.reportesDataI.first_page_url);
    }
    if (direction == "last") {
      res = await axios.get(state.reportesDataI.last_page_url);
    }
    commit("SET_REPORTESI_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}

export async function getReportesG({ commit, state }, params) {
  try {
    if (state.reportesDataG == null || params?.force) {
      const res = await api.get("/admin/reportesGrupales");
      console.log(res.data);
      commit("SET_REPORTESG_DATA", res.data);
    }
  } catch (error) {
    console.log(error);
  }
}

export async function changePaginationG({ commit, state }, direction) {
  console.log(direction);
  try {
    let res = [];
    if (direction == "next") {
      res = await axios.get(state.reportesDataG.next_page_url);
    }
    if (direction == "prev") {
      res = await axios.get(state.reportesDataG.prev_page_url);
    }
    if (direction == "first") {
      res = await axios.get(state.reportesDataG.first_page_url);
    }
    if (direction == "last") {
      res = await axios.get(state.reportesDataG.last_page_url);
    }
    commit("SET_REPORTESG_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
//* TUTORES
export async function getTutores({ commit, state }, params) {
  //force variable is boolean and forces commit and fetch
  try {
    if (state.tutores == null || params?.force) {
      const res = await api.get("/admin/tutores");
      console.log(res.data);
      commit("SET_TUTORES", res.data);
    }
  } catch (error) {
    console.log(error);
  }
}
export async function changePaginationT({ commit, state }, direction) {
  console.log(direction);
  try {
    let res = [];
    if (direction == "next") {
      res = await axios.get(state.tutores.next_page_url);
    }
    if (direction == "prev") {
      res = await axios.get(state.tutores.prev_page_url);
    }
    if (direction == "first") {
      res = await axios.get(state.tutores.first_page_url);
    }
    if (direction == "last") {
      res = await axios.get(state.tutores.last_page_url);
    }
    commit("SET_TUTORES", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function getTutor({}, id) {
  try {
    const res = await api.get(`/admin/tutores/${id}`);
    return res.data;
  } catch (error) {
    console.log(error);
  }
}
export async function createTutor({ commit }, tutor) {
  console.log(tutor);
  try {
    const res = await api.post("/admin/storeTutor", tutor);
    if (res.status == 200) {
      //!commit to tutores
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function updateTutor({ commit }, tutor) {
  console.log(tutor);
  try {
    const res = await api.post(`/admin/actualizarTutor/${tutor.id}`, tutor);
    if (res.status == 200) {
      //!commit to tutores
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function desactivarTutor({ commit }, tutor) {
  try {
    const res = await api.post(`/admin/tutores/desactivar/${tutor.id}`);
    if (res.status == 200) {
      console.log(res.data);
      commit("ADD_TUTOR_D", res.data);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
//* Tutores Desactivados
export async function getTutoresD({ commit, state }, params) {
  try {
    if (state.tutoresD == null || params?.force) {
      const res = await api.get("/admin/tutoresD");
      commit("SET_TUTORESD", res.data);
    }
  } catch (error) {
    console.log(error);
  }
}
export async function changePaginationTD({ commit, state }, direction) {
  try {
    let res = [];
    if (direction == "next") {
      res = await axios.get(state.tutoresD.next_page_url);
    }
    if (direction == "prev") {
      res = await axios.get(state.tutoresD.prev_page_url);
    }
    if (direction == "first") {
      res = await axios.get(state.tutoresD.first_page_url);
    }
    if (direction == "last") {
      res = await axios.get(state.tutoresD.last_page_url);
    }
    commit("SET_TUTORESD", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function activarTutor({ commit }, tutor) {
  try {
    const res = await api.post(`/admin/tutores/activar/${tutor.id}`);
    if (res.status == 200) {
      console.log(res.data);
      commit("REMOVE_TUTOR_D", res.data);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
//* Psicologos
export async function getPsicologos({ commit, state }, params) {
  try {
    if (state.psicologos == null || params?.force) {
      const res = await api.get("/admin/psicologos");
      commit("SET_PSICOLOGOS", res.data);
    }
  } catch (error) {
    console.log(error);
  }
}
export async function getPsicologo({}, id) {
  try {
    const res = await api.get(`/admin/psicologos/${id}`);
    return res.data;
  } catch (error) {
    console.log(error);
  }
}
export async function createPsicologo({ commit }, psicologo) {
  try {
    const res = await api.post("/admin/storePsicologo", psicologo);
    if (res.status == 200) {
      //!commit to tutores
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function updatePsicologo({ commit }, psicologo) {
  try {
    const res = await api.post(
      `/admin/actualizarPsicologo/${psicologo.id}`,
      psicologo
    );
    if (res.status == 200) {
      //!commit to psicologos
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function desactivarPsicologo({ commit }, tutor) {
  try {
    const res = await api.post(`/admin/psicologos/desactivar/${tutor.id}`);
    if (res.status == 200) {
      commit("ADD_PSICOLOGO_D", res.data);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}

//* Psicologos Desactivados
export async function getPsicologosD({ commit, state }, params) {
  try {
    if (state.psicologosD == null || params?.force) {
      const res = await api.get("/admin/psicologosD");
      commit("SET_PSICOLOGOSD", res.data);
    }
  } catch (error) {
    console.log(error);
  }
}
export async function activarPsicologo({ commit }, psicologo) {
  try {
    const res = await api.post(`/admin/psicologos/activar/${psicologo.id}`);
    if (res.status == 200) {
      console.log(res.data);
      commit("REMOVE_PSICOLOGO_D", res.data);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
//* Anuncios
export async function getAnuncios({ commit, state }, params) {
  try {
    if (state.anuncios == null || params?.force) {
      const res = await api.get("/anuncios");
      console.log(res.data);
      commit("SET_ANUNCIOS", res.data);
    }
  } catch (error) {
    console.log(error);
  }
}
export async function updateAnuncio({ commit }, anuncio) {
  // return console.log(anuncio);
  const anuncioF = convertToFormData(anuncio);
  try {
    const res = await api.post(
      `/admin/actualizarAnuncio/${anuncio.id}`,
      anuncioF,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
    if (res.status >= 200 && res.status < 300) {
      console.log(res.data, "here");
      console.log(res.status, "here");
      commit("UPDATE_ANUNCIO", res.data);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function getAnuncio({}, id) {
  try {
    const res = await api.get(`/anuncios/${id}`);
    return res.data;
  } catch (error) {
    console.log(error);
  }
}
export async function eliminarAnuncio({ commit }, anuncio) {
  try {
    const res = await api.post(`/admin/eliminarAnuncio/${anuncio.id}`);
    if (res.status == 200) {
      commit("REMOVE_ANUNCIO", res.data);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }S
}
export async function createAnuncio({ commit }, anuncio) {
  //! requires formdata because of the image
  const anuncioF = convertToFormData(anuncio);
  try {
    const res = await api.post("/admin/storeAnuncio", anuncioF, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    if (res.status >= 200 && res.status < 300) {
      commit("ADD_ANUNCIO", res.data);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function loadAnuncios({ state, commit }) {
  try {
    const res = await axios.get(state.anuncios.next_page_url);
    if (res.status >= 200 && res.status < 300) {
      commit("LOAD_ANUNCIOS", res.data);
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
}
//*Reset password

export async function resetUserPassword({}, resetData) {
  try {
    const res = await api.post("/resetP", resetData);
    if (res.status >= 200 && res.status < 300) {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
}

export async function getCanalizaciones({ commit }) {
  try {
    const res = await api.get("/admin/canalizaciones");
    console.log(res.data);
    if (res.status == 200) {
      commit("SET_CANALIZACIONES", res.data);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function canalizar({ commit }, canalizacion) {
  try {
    const res = await api.post("/admin/canalizar", canalizacion);
    console.log(res);
    if (res.status == 200) {
      commit("UPDATE_CANALIZACION", canalizacion);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

//* Administradores
export async function getAdministradores({ commit }) {
  try {
    const res = await api.get("/admin/administradores");
    commit("SET_ADMINISTRADORES", res.data);
  } catch (error) {
    console.log(error);
  }
}
