export function SET_REPORTESI_DATA(state, reportesData) {
  state.reportesDataI = reportesData;
}
export function SET_REPORTESG_DATA(state, reportesData) {
  state.reportesDataG = reportesData;
}

export function SET_TUTORES(state, tutores) {
  state.tutores = tutores;
}
export function SET_TUTORESD(state, tutores) {
  state.tutoresD = tutores;
}
export function ADD_TUTOR_D(state, tutor) {
  if (state.tutores) {
    state.tutores.data = state.tutores.data.filter((x) => x.id != tutor.id);
  }
  if (state.tutoresD) {
    state.tutoresD.data.unshift(tutor);
  }
}

export function REMOVE_TUTOR_D(state, tutor) {
  if (state.tutoresD) {
    state.tutoresD.data = state.tutoresD.data.filter((x) => x.id != tutor.id);
  }
  if (state.tutores) {
    state.tutores.data.unshift(tutor);
  }
}
export function SET_PSICOLOGOS(state, psicologos) {
  state.psicologos = psicologos;
}
export function SET_PSICOLOGOSD(state, psicologos) {
  state.psicologosD = psicologos;
}
export function ADD_PSICOLOGO_D(state, psicologo) {
  if (state.psicologos) {
    state.psicologos.data = state.psicologos.data.filter(
      (x) => x.id != psicologo.id
    );
  }
  if (state.psicologosD) {
    state.psicologosD.data.unshift(psicologo);
  }
}
export function REMOVE_PSICOLOGO_D(state, psicologo) {
  if (state.psicologosD) {
    state.psicologosD.data = state.psicologosD.data.filter(
      (x) => x.id != psicologo.id
    );
  }
  if (state.psicologo) {
    state.psicologo.data.unshift(psicologo);
  }
}
export function SET_ANUNCIOS(state, anuncios) {
  state.anuncios = anuncios;
}
export function ADD_ANUNCIO(state, anuncio) {
  if (state.anuncios.data) {
    state.anuncios.data.unshift(anuncio);
  }
}
export function UPDATE_ANUNCIO(state, anuncio) {
  if (state.anuncios?.data) {
    console.log("inside");
    state.anuncios.data = state.anuncios.data.map((x) =>
      x.id == anuncio.id ? anuncio : x
    );
  }
}
export function REMOVE_ANUNCIO(state, anuncio) {
  if (state.anuncios?.data) {
    state.anuncios.data = state.anuncios.data.filter((x) => x.id != anuncio.id);
  }
}
export function LOAD_ANUNCIOS(state, anuncios) {
  for (let i in anuncios.data) {
    state.anuncios.data.push(anuncios.data[i]);
  }
}

export function SET_CANALIZACIONES(state, canalizaciones) {
  state.canalizaciones = canalizaciones;
}
export function UPDATE_CANALIZACION(state, canalizacion) {
  console.log(canalizacion);
  state.canalizaciones.data = state.canalizaciones.data.map((x) =>
    x.id == canalizacion.reporte_id
      ? { ...x, psicologo_id: canalizacion.psicologo_id }
      : x
  );
}

export function SET_ADMINISTRADORES(state, administradores) {
  state.administradores = administradores;
}
