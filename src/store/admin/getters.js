//*REPORTES INDIVIDUALES

export function reportesI(state) {
  return state.reportesDataI?.data;
}

export function paginationObjectI(state) {
  const { data, ...pagination } = state.reportesDataI;
  return pagination;
}
export function paginationI(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectI.current_page,
    rowsPerPage: getters.paginationObjectI.per_page,
    rowsNumber: getters.paginationObjectI.total,
    totalPages: getters.paginationObjectI.last_page,
  };
  return pagination;
}
//* REPORTES GRUPALES
export function reportesG(state) {
  return state.reportesDataG?.data;
}

export function paginationObjectG(state) {
  const { data, ...pagination } = state.reportesDataG;
  return pagination;
}
export function paginationG(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectG.current_page,
    rowsPerPage: getters.paginationObjectG.per_page,
    rowsNumber: getters.paginationObjectG.total,
    totalPages: getters.paginationObjectG.last_page,
  };
  return pagination;
}

//* TUTORES
export function tutores(state) {
  return state.tutores?.data;
}

export function paginationObjectT(state) {
  const { data, ...pagination } = state.tutores;
  return pagination;
}
export function paginationT(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectT.current_page,
    rowsPerPage: getters.paginationObjectT.per_page,
    rowsNumber: getters.paginationObjectT.total,
    totalPages: getters.paginationObjectT.last_page,
  };
  return pagination;
}

//* PSICOLOGOS
export function psicologos(state) {
  return state.psicologos?.data;
}

export function paginationObjectP(state) {
  const { data, ...pagination } = state.psicologos;
  return pagination;
}
export function paginationP(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectP.current_page,
    rowsPerPage: getters.paginationObjectP.per_page,
    rowsNumber: getters.paginationObjectP.total,
    totalPages: getters.paginationObjectP.last_page,
  };
  return pagination;
}

//* ANUNCIOS

export function anuncios(state) {
  return state.anuncios?.data;
}

export function paginationObjectAnuncios(state) {
  const { data, ...pagination } = state.anuncios;
  return pagination;
}
export function paginationAnuncios(state, getters) {
  const pagination = {
    sortBy: "titulo",
    nameending: false,
    page: getters.paginationObjectAnuncios.current_page,
    rowsPerPage: getters.paginationObjectAnuncios.per_page,
    rowsNumber: getters.paginationObjectAnuncios.total,
    totalPages: getters.paginationObjectAnuncios.last_page,
  };
  return pagination;
}
//* TUTORES DESACTIVADOS
export function tutoresD(state) {
  return state.tutoresD?.data;
}

export function paginationObjectTD(state) {
  const { data, ...pagination } = state.tutoresD;
  return pagination;
}
export function paginationTD(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectTD.current_page,
    rowsPerPage: getters.paginationObjectTD.per_page,
    rowsNumber: getters.paginationObjectTD.total,
    totalPages: getters.paginationObjectTD.last_page,
  };
  return pagination;
}

//* PSICOLOGOS DESACTIVADOS
export function psicologosD(state) {
  return state.psicologosD?.data;
}

export function paginationObjectPD(state) {
  const { data, ...pagination } = state.psicologosD;
  return pagination;
}
export function paginationPD(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectPD.current_page,
    rowsPerPage: getters.paginationObjectPD.per_page,
    rowsNumber: getters.paginationObjectPD.total,
    totalPages: getters.paginationObjectPD.last_page,
  };
  return pagination;
}

//Canalizaciones
export function canalizaciones(state) {
  return state.canalizaciones?.data;
}

export function paginationObjectC(state) {
  const { data, ...pagination } = state.canalizaciones;
  return pagination;
}
export function paginationC(state, getters) {
  const pagination = {
    sortBy: "tutor",
    nameending: false,
    page: getters.paginationObjectC.current_page,
    rowsPerPage: getters.paginationObjectC.per_page,
    rowsNumber: getters.paginationObjectC.total,
    totalPages: getters.paginationObjectC.last_page,
  };
  return pagination;
}

export function administradores(state) {
  return state.administradores?.data;
}

export function paginationObjectA(state) {
  const { data, ...pagination } = state.administradores;
  return pagination;
}

export function paginationA(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectA.current_page,
    rowsPerPage: getters.paginationObjectA.per_page,
    rowsNumber: getters.paginationObjectA.total,
    totalPages: getters.paginationObjectA.last_page,
  };
  return pagination;
}
