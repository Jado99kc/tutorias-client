export function SET_ALUMNOST_DATA(state, alumnos) {
  state.alumnosT = alumnos;
}
export function SET_REPORTES_SESION_DATA(state, reportes) {
  state.reportesS = reportes;
}
export function SET_REPORTES_FINALES_DATA(state, reportes) {
  state.reportesF = reportes;
}
