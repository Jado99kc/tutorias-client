import { api, axios } from "src/boot/axios";
import { convertToFormData } from "src/helpers/formData";
import { downloadSesionPDF, downloadFinalPDF } from "src/helpers/MonitorPDF";

export async function getAlumnosT({ commit }) {
  try {
    const res = await api.get(`/monitor/alumnosTutorados`);
    commit("SET_ALUMNOST_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function getAlumnosTConId({ commit }, id) {
  try {
    const res = await api.get(`/monitor/${id}/alumnosTutorados`);
    commit("SET_ALUMNOST_DATA", res.data);
  } catch (error) {
    console.log(error);
  }
}
export async function crearReporteSesion({}, reporte) {
  const { evidencia, ...data } = reporte;
  const fd = new FormData();
  const extras = JSON.stringify({
    ...data,
  });
  fd.append("evidencia", evidencia);
  fd.append("data", extras);
  // return console.log(...fd);

  try {
    const res = await api.post("monitor/createReporteSesion", fd, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    if (res.status >= 200 && res.status < 300) {
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
    // throw error;
    return false;
  }
}

export async function getReportesSesion({ commit }) {
  try {
    const res = await api.get(`/monitor/reportesSesion`);
    commit("SET_REPORTES_SESION_DATA", res.data);
  } catch (error) {
    console.log(error);
    throw error;
  }
}
export async function getReporteSesion({}, id) {
  try {
    const res = await api.get(`/monitor/reporteSesion/${id}`);
    console.log(res);
    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
}
export async function removeAsignatura({}, id) {
  try {
    const res = await api.post(`/monitor/removeRsAsignatura/${id}`);
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function removeContenido({}, id) {
  try {
    const res = await api.post(`/monitor/removeRsContenido/${id}`);
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function removeAsistente({}, id) {
  try {
    const res = await api.post(`/monitor/removeRsAsistente/${id}`);
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function printReporteSesion({}, reporte) {
  try {
    const res = await api.get(`/monitor/printReporteSesion/${reporte.id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      downloadSesionPDF(res.data, reporte);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function updateReporteSesion({}, reporte) {
  const { evidencia, ...data } = reporte;
  const fd = new FormData();
  const extras = JSON.stringify({
    ...data,
  });
  fd.append("evidencia", evidencia);
  fd.append("data", extras);
  try {
    const res = await api.post(
      `/monitor/updateReporteSesion/${reporte.id}`,
      fd,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
    return res.status == 200;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function previewReporteSesion({}, id) {
  try {
    const res = await api.get(`/monitor/printReporteSesion/${id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      return URL.createObjectURL(res.data);
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function crearReporteFinal({}, reporte) {
  console.log(reporte);
  try {
    const res = await api.post(`/monitor/createReporteFinal`, reporte);
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function getReportesFinales({ commit }) {
  try {
    const res = await api.get(`/monitor/reportesFinales`);
    commit("SET_REPORTES_FINALES_DATA", res.data);
  } catch (error) {
    console.log(error);
    throw error;
  }
}
export async function getReporteFinal({}, id) {
  try {
    const res = await api.get(`/monitor/reporteFinal/${id}`);
    console.log(res);
    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
}
export async function printReporteFinal({}, reporte) {
  try {
    const res = await api.get(`/monitor/printReporteFinal/${reporte.id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      downloadFinalPDF(res.data, reporte);
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function removeRfAsistente({}, id) {
  try {
    const res = await api.post(`/monitor/removeRfAsistente/${id}`);
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function removeRfContenido({}, id) {
  try {
    const res = await api.post(`/monitor/removeRfContenido/${id}`);
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function updateReporteFinal({}, reporte) {
  try {
    const res = await api.post(
      `/monitor/updateReporteFinal/${reporte.id}`,
      reporte
    );
    if (res.status == 200) {
      return true;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
export async function previewReporteFinal({}, id) {
  try {
    const res = await api.get(`/monitor/printReporteFinal/${id}`, {
      responseType: "blob",
    });
    if (res.status == 200) {
      return URL.createObjectURL(res.data);
    }
  } catch (error) {
    console.log(error);
    return false;
  }
}
