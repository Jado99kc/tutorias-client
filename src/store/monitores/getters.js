//* Monitores
export function alumnosT(state) {
  return state.alumnosT?.data;
}

export function paginationObjectAT(state) {
  const { data, ...pagination } = state.alumnosT;
  return pagination;
}
export function paginationAT(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectAT.current_page,
    rowsPerPage: getters.paginationObjectAT.per_page,
    rowsNumber: getters.paginationObjectAT.total,
    totalPages: getters.paginationObjectAT.last_page,
  };
  return pagination;
}

export function reportesS(state) {
  return state.reportesS?.data;
}

export function paginationObjectRS(state) {
  const { data, ...pagination } = state.reportesS;
  return pagination;
}
export function paginationRS(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectRS.current_page,
    rowsPerPage: getters.paginationObjectRS.per_page,
    rowsNumber: getters.paginationObjectRS.total,
    totalPages: getters.paginationObjectRS.last_page,
  };
  return pagination;
}

export function reportesF(state) {
  return state.reportesF?.data;
}

export function paginationObjectRF(state) {
  const { data, ...pagination } = state.reportesF;
  return pagination;
}

export function paginationRF(state, getters) {
  const pagination = {
    sortBy: "nombres",
    nameending: false,
    page: getters.paginationObjectRF.current_page,
    rowsPerPage: getters.paginationObjectRF.per_page,
    rowsNumber: getters.paginationObjectRF.total,
    totalPages: getters.paginationObjectRF.last_page,
  };
  return pagination;
}
