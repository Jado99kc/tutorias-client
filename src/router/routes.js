const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    meta: { private: true },
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "/about", component: () => import("pages/About.vue") },
      {
        path: "/admin/administradores",
        component: () => import("pages/Admin/Administradores/Index.vue"),
      },
      //*Anuncios
      {
        path: "/anuncios",
        component: () => import("pages/Admin/Anuncios/Index.vue"),
      },
      {
        path: "/anuncios/crear",
        component: () => import("src/pages/Admin/Anuncios/Create.vue"),
      },
      {
        path: "/anuncios/actualizar/:id",
        component: () => import("src/pages/Admin/Anuncios/Update.vue"),
      },
      //Psicologos
      {
        path: "/psicologos",
        component: () => import("pages/Admin/Psicologos/Index.vue"),
      },
      {
        path: "/psicologos/desactivados",
        component: () => import("pages/Admin/Psicologos/Desactivados.vue"),
      },
      {
        path: "/psicologos/crear",
        component: () => import("pages/Admin/Psicologos/Crear.vue"),
      },
      {
        path: "/psicologos/canalizaciones",
        component: () => import("pages/Psicologos/Canalizaciones/Index.vue"),
      },
      {
        path: "/psicologos/actualizar/:id",
        component: () => import("src/pages/Admin/Psicologos/Update.vue"),
      },
      //*Tutores
      {
        path: "/tutores",
        component: () => import("pages/Admin/Tutores/Index.vue"),
      },
      {
        path: "/tutores/desactivados",
        component: () => import("pages/Admin/Tutores/Desactivados.vue"),
      },
      {
        path: "/tutores/crear",
        component: () => import("src/pages/Admin/Tutores/Create.vue"),
      },
      {
        path: "/tutores/actualizar/:id",
        component: () => import("src/pages/Admin/Tutores/Update.vue"),
      },
      {
        path: "/tutores/:id/reportesIndividuales",
        component: () => import("pages/Tutores/ReportesIndividuales/Index.vue"),
      },
      {
        path: "/tutores/:id/reportesGrupales",
        component: () => import("pages/Tutores/ReportesGrupales/Index.vue"),
      },
      {
        path: "/tutores/:id/alumnosMonitores",
        component: () => import("src/pages/Tutores/AlumnosMonitores/Index.vue"),
      },

      //Reportes
      {
        path: "/reportesI",
        component: () => import("pages/Admin/Reportes/Individuales/index.vue"),
      },
      {
        path: "/reportesI/:id",
        component: () => import("pages/Admin/Reportes/Individuales/_id.vue"),
      },
      {
        path: "/reportesG",
        component: () => import("pages/Admin/Reportes/Grupales/index.vue"),
      },
      {
        path: "/reportesG/:id",
        component: () => import("pages/Admin/Reportes/Grupales/_id.vue"),
      },
      //? Canalizaciones
      {
        path: "/admin/canalizaciones",
        component: () => import("pages/Admin/Canalizaciones/Index.vue"),
      },

      //*Rutas de tutor
      //?Perfil
      {
        path: "/tutores/perfil",
        component: () => import("pages/Tutores/Perfil.vue"),
      },
      //?Monitores
      {
        path: "/tutores/alumnosM/crear",
        component: () => import("pages/Tutores/AlumnosMonitores/Crear.vue"),
      },
      {
        path: "/tutores/alumnosM",
        component: () => import("pages/Tutores/AlumnosMonitores/Index.vue"),
      },
      {
        path: "/tutores/alumnosM/:id",
        component: () => import("pages/Tutores/AlumnosMonitores/_Id.vue"),
      },
      {
        path: "/tutores/alumnosMD",
        component: () =>
          import("pages/Tutores/CuentasDesactivadas/Monitores.vue"),
      },
      //?Tutorados
      {
        path: "/tutores/alumnosT/crear",
        component: () => import("pages/Tutores/AlumnosTutorados/Crear.vue"),
      },
      {
        path: "/tutores/alumnosT",
        component: () => import("pages/Tutores/AlumnosTutorados/Index.vue"),
      },
      {
        path: "/tutores/alumnosT/:id",
        component: () => import("pages/Tutores/AlumnosTutorados/_Id.vue"),
      },
      {
        path: "/tutores/alumnosTD",
        component: () =>
          import("pages/Tutores/CuentasDesactivadas/Tutorados.vue"),
      },
      //? Reportes Individuales
      {
        path: "/tutores/reportesI",
        component: () => import("pages/Tutores/ReportesIndividuales/Index.vue"),
      },
      {
        path: "/tutores/reportesI/crear",
        component: () => import("pages/Tutores/ReportesIndividuales/Crear.vue"),
      },
      {
        path: "/tutores/reportesI/update/:id",
        component: () =>
          import("src/pages/Tutores/ReportesIndividuales/Update.vue"),
      },
      {
        // visualizar pdf
        path: "/tutores/reportesI/:id",
        component: () =>
          import("src/pages/Tutores/ReportesIndividuales/_Id.vue"),
      },
      //? Reportes Grupales
      {
        path: "/tutores/reportesG",
        component: () => import("pages/Tutores/ReportesGrupales/Index.vue"),
      },
      {
        path: "/tutores/reportesG/crear",
        component: () => import("pages/Tutores/ReportesGrupales/Crear.vue"),
      },
      {
        path: "/tutores/reportesG/update/:id",
        component: () =>
          import("src/pages/Tutores/ReportesGrupales/Update.vue"),
      },
      {
        // visualizar pdf
        path: "/tutores/reportesG/:id",
        component: () => import("src/pages/Tutores/ReportesGrupales/_Id.vue"),
      },
      //? Reportes Parciales
      {
        path: "/tutores/reportesP/crear",
        component: () =>
          import("src/pages/Tutores/ReportesParciales/Crear.vue"),
      },
      {
        path: "/tutores/reportesP",
        component: () =>
          import("src/pages/Tutores/ReportesParciales/Index.vue"),
      },
      {
        path: "/tutores/:id/reportesP",
        component: () =>
          import("src/pages/Tutores/ReportesParciales/Index.vue"),
      },
      {
        // visualizar pdf
        path: "/tutores/reportesP/:id",
        component: () => import("src/pages/Tutores/ReportesParciales/_Id.vue"),
      },
      {
        path: "/tutores/reportesP/editar/:id",
        component: () =>
          import("src/pages/Tutores/ReportesParciales/Editar.vue"),
      },
      //? Reportes Finales Monitores
      {
        path: "/tutores/reportesFinalesMonitor",
        component: () =>
          import("src/pages/Tutores/ReportesFinalesMonitor/Index.vue"),
      },
      {
        path: "/tutores/reportesFinalesMonitor/update/:id",
        component: () =>
          import("pages/Tutores/ReportesFinalesMonitor/Update.vue"),
      },
      //* Rutas de Monitor
      {
        path: "/monitores/alumnosTutorados",
        component: () => import("pages/Monitores/AlumnosTutorados/Index.vue"),
      },
      {
        path: "/monitores/:id/alumnosTutorados",
        component: () => import("pages/Monitores/AlumnosTutorados/Index.vue"),
      },
      {
        path: "/monitores/reportesSesion/crear",
        component: () => import("pages/Monitores/ReportesSesion/Crear.vue"),
      },
      {
        path: "/monitores/reportesSesion",
        component: () => import("pages/Monitores/ReportesSesion/Index.vue"),
      },
      {
        path: "/monitores/reportesSesion/update/:id",
        component: () => import("pages/Monitores/ReportesSesion/Update.vue"),
      },
      {
        // visualizar pdf
        path: "/monitores/reportesSesion/:id",
        component: () => import("src/pages/Monitores/ReportesSesion/_Id.vue"),
      },
      {
        path: "/monitores/reportesFinales/crear",
        component: () => import("pages/Monitores/ReportesFinales/Crear.vue"),
      },
      {
        path: "/monitores/reportesFinales",
        component: () => import("pages/Monitores/ReportesFinales/Index.vue"),
      },
      {
        path: "/monitores/reportesFinales/update/:id",
        component: () => import("pages/Monitores/ReportesFinales/Update.vue"),
      },
      {
        // visualizar pdf
        path: "/monitores/reportesFinales/:id",
        component: () => import("src/pages/Monitores/ReportesFinales/_Id.vue"),
      },

      //!Psicologos
      {
        path: "/psicologos/entrevistas",
        component: () => import("pages/Psicologos/Entrevistas/Index.vue"),
      },
      {
        path: "/psicologos/entrevistas/subir_bajar",
        component: () => import("pages/Psicologos/Entrevistas/Subir_Bajar.vue"),
      },
      {
        path: "/psicologos/reportesFinales",
        component: () => import("pages/Psicologos/ReportesFinales/Index.vue"),
      },
      {
        path: "/psicologos/reportesFinales/crear",
        component: () => import("pages/Psicologos/ReportesFinales/Crear.vue"),
      },
      {
        path: "/psicologos/reportesFinales/update/:id",
        component: () => import("pages/Psicologos/ReportesFinales/Update.vue"),
      },
    ],
  },
  {
    path: "/login",
    component: () => import("layouts/PublicLayout.vue"),
    children: [{ path: "", component: () => import("pages/auth/Login.vue") }],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
